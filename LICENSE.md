The files in this repository, unless otherwise noted, are licensed under [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) License.
