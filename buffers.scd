p = ProxySpace.push(s); p.gui;

// Load Buffers
(
b = "samples/stereo/*.ogg".resolveRelative.pathMatch.collect { |path|
	Buffer.read(s, path);
};
);

// Define Synth
(
SynthDef(\bsnip, {
	| out = 0, amp = 1, buf, rate = 1, pos = 0.5, d = 0.1 |
	var env = Env.perc(releaseTime: d).ar(2);
	var sig = BufRd.ar(2, buf, Phasor.ar(start: pos * BufFrames.kr(buf)));
	sig = env * sig;
	Out.ar(out, sig * amp);
}).add;
);

// Define Process
(
~p1 = Pbind(
	\instrument, \bsnip,
	\buf, b.choose.postln,
	\dur, 0.5,
	\d, 0.5,
	\pos, Pwhite(0.4, 0.6),
	\amp, 0.5,
);
);

// Some reverb
(
~p1rev = { FreeVerb.ar(~p1.ar, room: 0.7); }
);
